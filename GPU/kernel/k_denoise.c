/*
   The MIT License (MIT)

   Copyright (c) 2014 Sagar G V (sagar.writeme@gmail.com)

   Permission is hereby granted, free of charge, to any person obtaining a copy of
   this software and associated documentation files (the "Software"), to deal in
   the Software without restriction, including without limitation the rights to
   use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
   the Software, and to permit persons to whom the Software is furnished to do so,
   subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
   FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
   COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */


__kernel void identity(__global const float *d_ip, __global float *d_op) {

    // Get the index of the current element to be processed
    int i = get_global_id(0);

    // Do the operation
    d_op[i] = d_ip[i];
}

__kernel void gaussian(__global const float *d_sigmaVect, __global const float *d_theta, __global float *d_filter,
        __global float *d_filter_dx, __global float *d_filter_dy, 
        int n_w, int n_h, int filter_size) {
    int i = get_global_id(0);
    int j = get_global_id(1);

    if(i >= n_w || j >= n_h)
        return;

    int x, y;
    float x_theta, y_theta, x_theta2, y_theta2, val;

    int N = filter_size;
    float theta = d_theta[i + j*n_w];
    float sigmax = d_sigmaVect[2*(i + j*n_w)];
    float sigmay = d_sigmaVect[2*(i + j*n_w)+1];
    
    float sigmax2 = sigmax*sigmax;
    float sigmay2 = sigmay*sigmay;

    float sigmax3 = sigmax2 * sigmax;
    float sigmay3 = sigmay2 * sigmay;

    float acc, acc_dx, acc_dy;
    acc = acc_dx = acc_dy = 0;

    for(y = -N/2; y <= N/2; y++) {
        for(x = -N/2; x <= N/2; x++) {
            //////////////////////////////////////////////
            x_theta = x*cos(theta) + y*sin(theta);
            y_theta = -x*sin(theta) + y*cos(theta);
            x_theta2 = x_theta * x_theta;
            y_theta2 = y_theta * y_theta;

            val = exp(-0.5*( x_theta2/sigmax2 + y_theta2/sigmay2 ));
            d_filter[(i + j*n_w)*(N*N) + (x + (N/2)) + (y + (N/2))*N] = val;
            acc += val;
            //////////////////////////////////////////////

            d_filter_dx[(i + j*n_w)*(N*N) + (x + (N/2)) + (y + (N/2))*N] = val;
            acc_dx += val * x_theta2/sigmax3;
            //////////////////////////////////////////////

            d_filter_dy[(i + j*n_w)*(N*N) + (x + (N/2)) + (y + (N/2))*N] = val;
            acc_dy += val * y_theta2/sigmay3;

        }
    }
    float acc2 = acc * acc;
    for(y = -N/2; y <= N/2; y++) {
        for(x = -N/2; x <= N/2; x++) {
            x_theta = x*cos(theta) + y*sin(theta);
            y_theta = -x*sin(theta) + y*cos(theta);
            x_theta2 = x_theta * x_theta;
            y_theta2 = y_theta * y_theta;

            d_filter[(i + j*n_w)*(N*N) + (x + (N/2)) + (y + (N/2))*N] /= acc;
            d_filter_dx[(i + j*n_w)*(N*N) + (x + (N/2)) + (y + (N/2))*N] *= (acc * x_theta2/sigmax3 - acc_dx)/acc2;
            d_filter_dy[(i + j*n_w)*(N*N) + (x + (N/2)) + (y + (N/2))*N] *= (acc * y_theta2/sigmay3 - acc_dy)/acc2;
        }
    }
}

float conv(__global const float *d_eimg, __global const float *d_h, int x, int y, int N, int offset, int ew)
{
    float acc = 0;
    int i, j;
    for (j = -N/2; j <= N/2; j++) {
        for(i = -N/2; i <= N/2; i++)
        {
            acc += d_eimg[(x-i+offset) + (y-j+offset)*ew] * d_h[(i + N/2) + (j + N/2)*N];
        }
    }
    
    return acc;
}

__kernel void costimg(__global const float* d_eimg, 
        __global const float* d_filt, __global const float *d_filt_dx, __global const float *d_filt_dy,
        __global float *d_costimg, __global float *d_costimg_dx, __global float *d_costimg_dy,
        int n_w, int n_h, int filter_size, int eblock_size, int block_size, int offset, int ew, int apron_size)
{
    int i = get_global_id(0) / eblock_size;
    int j = get_global_id(1) / eblock_size;

    if(i >= n_w || j >= n_h)
        return;

    int x = get_global_id(0) - i*eblock_size;
    int y = get_global_id(1) - j*eblock_size;

    // x should run from (i*block_size - apron_size) to ( (i+1)*block_size + apron_size )
    // but it is running from 0 to block_size+2*apron_size (which is eblock_size). So lets, fix that.
    x += i*block_size - apron_size;
    y += j*block_size - apron_size;

    int xd = get_global_id(0);
    int yd = get_global_id(1);

    float filt, filtdx, filtdy;

    float samp = d_eimg[(x+offset) + (y+offset)*ew];

    filt = conv(d_eimg, &d_filt[(i + j*n_w)*filter_size*filter_size], x, y, filter_size, offset, ew); 
    d_costimg[xd + yd*n_w*eblock_size] = filt*(filt - 2*samp); 


    filtdx = conv(d_eimg, &d_filt_dx[(i + j*n_w)*filter_size*filter_size], x, y, filter_size, offset, ew); 
    d_costimg_dx[xd + yd*n_w*eblock_size] = 2*filtdx*(filt - samp); 

    filtdy = conv(d_eimg, &d_filt_dy[(i + j*n_w)*filter_size*filter_size], x, y, filter_size, offset, ew); 
    d_costimg_dy[xd + yd*n_w*eblock_size] = 2*filtdy*(filt - samp); 

}


__kernel void blockreduce(__global float* costimg, __global float* cost, int w, int h, int n_w, int n_h, int block_size,
                            __global float* filt, int filt_size, float sigma,
                            __global float* costimg_dx, __global float* cost_dx, __global float* filt_dx,
                            __global float* costimg_dy, __global float* cost_dy, __global float* filt_dy)
{
    int i = get_global_id(0);
    int j = get_global_id(1);

    if(i >= n_w || j >= n_h)
        return; 

    cost[i + j*n_w] = 0;
    cost_dx[i + j*n_w] = 0;
    cost_dy[i + j*n_w] = 0;
    
    int x, y;
    for(y = j*block_size; y < (j+1)*block_size; y++)
    {
        for(x = i*block_size; x < (i+1)*block_size; x++)
        {
            cost[i + j*n_w] += costimg[x + y*w];
            cost_dx[i + j*n_w] += costimg_dx[x + y*w];
            cost_dy[i + j*n_w] += costimg_dy[x + y*w];
        }
    }
    cost[i + j*n_w] += 2*(sigma*sigma)*(block_size*block_size)*filt[(i+j*n_w)*filt_size*filt_size + filt_size/2 + (filt_size/2)*filt_size];
    cost_dx[i + j*n_w] += 2*sigma*sigma*block_size*block_size*filt_dx[(i+j*n_w)*filt_size*filt_size + filt_size/2 + (filt_size/2)*filt_size];
    cost_dy[i + j*n_w] += 2*sigma*sigma*block_size*block_size*filt_dy[(i+j*n_w)*filt_size*filt_size + filt_size/2 + (filt_size/2)*filt_size];
    
}
