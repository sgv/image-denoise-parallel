#ifndef __FMINCG_C_H__
#define __FMINCG_C_H__

#include <float.h>
#include <math.h>

#define COST_FUNC_DATATYPE float
#define COST_FUNC_DATATYPE_MIN (FLT_MIN*100)

#define RHO 0.01f
#define SIG 0.5f
#define INT 0.1f
#define EXT 3.0f
#define MAX_CG 20
#define RATIO 100.0f

#define nDim 2


// iState should be able to hold 3 integers
// state should be able to hold (15+6*nDim) COST_FUNC_DATATYPEs
// initialize xVector to the initial vector (to start searching for the minimum)
// initialize all the other arguments to 0.
// memory for all pointers should be allocated by caller and other than xVector, others should be initialized to 0.
// iterate like this:
//      xVector = {2, 3}(init_vector); cost=0;gradVector={0,0};iState=0..0;state=0..0;
//      for(i=0;i<max_cost_func_calls;i++) {
//          fmincg(xVector, cost, gradVector, iState, state);
//          cost = costFunc(xVector);
//          gradVector = costFuncGrad(xVector);
//      }
//      printf("Function is minimized at %f", xVector); // xVector has the minimum
void fmincg(COST_FUNC_DATATYPE* xVector,COST_FUNC_DATATYPE* cost,COST_FUNC_DATATYPE* gradVector,int* iState,COST_FUNC_DATATYPE* state);

#endif
