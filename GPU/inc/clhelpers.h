#ifndef __CLHELPERS_H__
#define __CLHELPERS_H__

#include <CL/cl.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_SOURCE_SIZE 1000000UL

int clamp2(int x);

// caller has to allocate memory for pointer arguments.
void clInit(cl_context* context, cl_command_queue* command_queue, int devNum);

cl_program clhBuildProgram(cl_context context, cl_command_queue command_queue, const char* fileName);

cl_mem clhMalloc(cl_context context, size_t size);

void clhHostToDeviceWait(cl_command_queue command_queue, cl_mem d_mem, void* h_mem, size_t size);
void clhDeviceToHostWait(cl_command_queue queue, cl_mem d_mem, void* h_mem, size_t size);

cl_kernel clhCreateKernel(cl_program program, const char* kernelName); 

void clhSetArg(cl_kernel k, int n, size_t size, const void* arg);
void clhKernelCall2D(cl_command_queue command_queue, cl_kernel kernel, int w, int h); // get_global_size(0) => w; get_global_size(1) => h
void clhKernelCall2Ds(cl_command_queue command_queue, cl_kernel kernel, int l_w, int l_h, int g_w, int g_h);

#endif
