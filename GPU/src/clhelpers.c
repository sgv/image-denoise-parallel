/*
The MIT License (MIT)

Copyright (c) 2014 Sagar G V (sagar.writeme@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "clhelpers.h"

extern int verbosity;
cl_device_id device_ids[10]; // Hard coded max devices = 10
int clDev;

int clamp2(int x) {
    return ((int)pow(2, ceil(log(x)/log(2))));
}

void clhSetArg(cl_kernel k, int n, size_t size, const void* arg) {
    if( clSetKernelArg(k, n, size, arg) != CL_SUCCESS ) { 
        fprintf(stderr, "Setting argument %d failed\n", n); 
        exit(10); 
    } 
}

cl_mem clhMalloc(cl_context context, size_t size) {
    cl_int ret;
    cl_mem d_var = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &ret);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Out of device memory!\n");
        exit(4);
    }
    return d_var;
}

void clhKernelCall2D(cl_command_queue command_queue, cl_kernel kernel, int w, int h) {
    cl_int ret;
    size_t global_item_size[2] = {w, h};
    ret = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL, global_item_size, NULL, 0, NULL, NULL);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Error calling openCL kernel. Errno: %d\n", ret);
        exit(11);
    }
}

void clhKernelCall2Ds(cl_command_queue command_queue, cl_kernel kernel, int l_w, int l_h, int g_w, int g_h) {
    cl_int ret;
    size_t global_work_size[2] = {g_w, g_h};
    size_t local_work_size[2] = {l_w, l_h};
    ret = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL, global_work_size, local_work_size, 0, NULL, NULL);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Error calling openCL kernel. Errno: %d\n", ret);
        exit(11);
    }
}

void clhHostToDeviceWait(cl_command_queue command_queue, cl_mem d_mem, void* h_mem, size_t size) {
    cl_int ret;
    ret = clEnqueueWriteBuffer(command_queue, d_mem, CL_TRUE, 0, size, h_mem, 0, NULL, NULL);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Could not transfer data from device to host\n");
        exit(8);
    }
}

cl_kernel clhCreateKernel(cl_program program, const char* kernelName) {
    cl_int ret;
    cl_kernel k = clCreateKernel(program, kernelName, &ret);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Failed obtain function '%s' from the kernel\n", kernelName);
        exit(9);
    }
    return k;
}

void clhDeviceToHostWait(cl_command_queue queue, cl_mem d_mem, void* h_mem, size_t size) {
    cl_int ret;
    ret = clEnqueueReadBuffer(queue, d_mem, CL_TRUE, 0, size, h_mem, 0, NULL, NULL);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Failed to transfer data from device memory to main memory\n");
        exit(8);
    }
}

void clInit(cl_context* context, cl_command_queue* command_queue, int openCLdev) {
    // Get platform and device information
    cl_platform_id platform_id = NULL;
    clDev = openCLdev;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret;

    ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
    ret = clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, 10, device_ids, &ret_num_devices);
    if(openCLdev >= ret_num_devices) {
        openCLdev = ret_num_devices - 1;
        clDev = openCLdev;
    }
    if(verbosity >= 1) {
        char cBuffer[100];
        printf("Num of OpenCL devices found: %d\n", ret_num_devices);
        clGetDeviceInfo(device_ids[openCLdev], CL_DEVICE_NAME, 99, &cBuffer, NULL);
        printf("Selected device: %s\n", cBuffer);
    }

    // Create an OpenCL context
    *context = clCreateContext( NULL, 1, device_ids+openCLdev, NULL, NULL, &ret);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Could not create OpenCL context\n");
        exit(3);
    }
    // Create a command queue
    *command_queue = clCreateCommandQueue(*context, device_ids[openCLdev], CL_QUEUE_PROFILING_ENABLE, &ret);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Could not crate OpenCL command queue\n");
        exit(3);
    }
    
}

cl_program clhBuildProgram(cl_context context, cl_command_queue command_queue, const char* fileName) {
    // Load the kernel source code into the array source_str
    cl_int ret;
    FILE *fp;
    char *source_str;
    size_t source_size;

    fp = fopen(fileName, "r");
    if (!fp) {
        fprintf(stderr, "Failed to load kernel file.\n");
        exit(1);
    }
    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    if(!source_str) {
        fprintf(stderr, "Out of memory\n");
        exit(1);
    }
    source_size = fread( source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose( fp );
    // Create a program from the kernel source
    cl_program program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Could not create openCL kernel program.\n");
        exit(6);
    }
    // Build the program
    ret = clBuildProgram(program, 1, device_ids+clDev, NULL, NULL, NULL);
    if(ret != CL_SUCCESS) {
        fprintf(stderr, "Could not build openCL kernel. Err = %d\nBuild Log:\n\n", ret);
        // Determine the size of the log
        size_t log_size;
        clGetProgramBuildInfo(program, device_ids[clDev], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

        // Allocate memory for the log
        char *log = (char *) malloc(log_size);

        // Get the log
        clGetProgramBuildInfo(program, device_ids[clDev], CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

        // Print the log
        fprintf(stderr, "%s\n", log);
        exit(7);
    }
    free(source_str);
    return program;
}
