/*
   The MIT License (MIT)

   Copyright (c) 2014 Sagar G V (sagar.writeme@gmail.com)

   Permission is hereby granted, free of charge, to any person obtaining a copy of
   this software and associated documentation files (the "Software"), to deal in
   the Software without restriction, including without limitation the rights to
   use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
   the Software, and to permit persons to whom the Software is furnished to do so,
   subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
   FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
   COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */

#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "denoise.h"
#include "fmincg.h"
#include "clhelpers.h"

cl_context context;
cl_command_queue command_queue;
cl_kernel k_identity, k_gaussian, k_costimg;


void test_mirror();
void test_imgExpand();
void test_conv();
void test_estimNoise();
void test_stTensorOrient();
void test_createGaussian();
void test_cost();
void test_fmincg();

void test_k_gaussian();
void test_k_costimg();
void test_k_blockreduce();

float eps = 1e-3;

float mirror(float* img, int w, int h, int x, int y);

#define DEBUGF(x) printf("\nDEBUG: %.4f\n", x);

int verbosity=0;
cl_context context;
cl_command_queue command_queue;
cl_kernel k_identity, k_gaussian, k_costimg, k_blockreduce;


int main()
{
    test_mirror();
    test_imgExpand();
    test_conv();
    test_estimNoise();
    test_stTensorOrient();
    test_createGaussian();
    test_cost();
    test_fmincg();

    int openCLdev = 0;
    clInit(&context, &command_queue, openCLdev);
    cl_program program = clhBuildProgram(context, command_queue, "kernel/k_denoise.c");
    k_identity = clhCreateKernel(program, "identity");
    k_gaussian = clhCreateKernel(program, "gaussian");
    k_costimg = clhCreateKernel(program, "costimg");
    k_blockreduce = clhCreateKernel(program, "blockreduce");

    test_k_gaussian();
    test_k_costimg();
    test_k_blockreduce();

    return 0;
}

void test_mirror()
{
    printf("Testing mirror() . . . ");
    // For this 2x2 matrix, mirror(-1,0) == 3, and mirror(3,2) == 3
    float arr[] = { 1, 3, 9,
        8, 3, 7 };
    assert(mirror(arr, 3, 2, -1, 0) == 3);
    assert(mirror(arr, 3, 2, 3, 2) == 3);
    printf("OK !\n");
}
void test_imgExpand() 
{
    printf("Testing imgExpand() . . . ");
    float arr[] = { 1, 3, 9,
        8, 3, 7 };
    // For this 2x2 matrix, with 
    // imgExpand with new_width=4, new_height=2, offset=1,
    // we should be getting
    // | 3 8 3 7 3 8 |
    // | 3 1 3 9 3 1 |
    // | 3 8 3 7 3 8 |
    // | 3 1 3 9 3 1 |
    float* earr = malloc((4+2)*(2+2)*sizeof(float));
    assert(earr != 0);

    imgExpand(arr, 3, 2, 4, 2, 1, earr);

    assert(earr[0] == 3);
    assert(earr[5] == 8);
    assert(earr[6] == 3);
    assert(earr[1] == 8);
    assert(earr[7] == 1);
    assert(earr[23] == 1);

    printf("OK !\n");
}

void test_conv() 
{
    printf("Testing conv() . . . ");
    float eimg[] = {
        3, 8, 3, 7, 3, 8,
        3, 1, 3, 9, 3, 1,
        3, 8, 3, 7, 3, 8,
        3, 1, 3, 9, 3, 1  }; // image (6,4) with offset = 1
    float mask[] = {
        1, 1, 1,
        1, 1, -1,
        1, 1, 1  };

    assert(fabs(conv(eimg, 6, 4, 1, 2, 1, mask, 3) - 37) < eps);
    assert(fabs(conv(eimg, 6, 4, 1, 0, 0, mask, 3) - 29) < eps);

    printf("OK !\n");
}

void test_estimNoise()
{
    printf("Testing estimNoise() . . . ");
    // original
    // |3 2|
    // |4 1|
    float eimg[] = {
        1, 4, 1, 4,
        2, 3, 2, 3,
        1, 4, 1, 4,
        2, 3, 2, 3 }; // w=2, h=2, ew=4, eh=4, offset=1
    // After filtering
    // (1/9) * | 8  8  |
    //         | 12 16 |
    // After absing
    // { (8/9), (8/9), (16/9), (16/9) }
    // Median = (8/9)
    // Noise estimate = (8/9) / 0.675 = 1.37280137
    assert(fabs(estimNoise(eimg, 4, 4, 2, 2, 1) - 1.37280137) < eps);

    printf("OK !\n");
}

void test_stTensorOrient()
{
    printf("Testing stTensorOrient() . . . ");
    float eimg[] = {
        3, 8, 3, 7, 3, 8,
        3, 1, 3, 9, 3, 1,
        3, 8, 3, 7, 3, 8,
        3, 1, 3, 9, 3, 1  }; // original image size = (3,2), block_size = 2, offset = 1. big size = (6,4)

    // x-deriv = | 0  74  0 |
    //           | 0  38  0 | 
    //
    // y-deriv = | 0   0  0 |
    //           | 0   0  0 |
    // xx = 74^2 + 38^2
    // xy = 0
    // yy = 0
    // Orient = PI/2 + 0.5*atan(2 * 0 / (xx - yy)) = PI/2
    assert(fabs(stTensorOrient(eimg, 6, 4, 1, 0, 0, 3, 2) - 1.5707963267948966) < eps);

    printf("OK !\n");
}

void test_createGaussian()
{
    printf("Testing createGaussian() . . . ");
    float h[9];
    createGaussian(h, 3, 1, 1, 0);
    // h should be  | 0.0751    0.1238    0.0751 |
    //              | 0.1238    0.2042    0.1238 |
    //              | 0.0751    0.1238    0.0751 |
    //
    assert(fabs(h[4] - 0.2042) < eps);
    assert(fabs(h[0] - 0.0751) < eps);
    assert(fabs(h[7] - 0.1238) < eps);
    assert(fabs(h[8] - 0.0751) < eps);
    printf("OK !\n");
}
void test_cost()
{
    printf("Testing cost() . . . ");
    // original
    // y = |3 2|
    //     |4 1|
    float eimg[] = {
        1, 4, 1, 4,
        2, 3, 2, 3,
        1, 4, 1, 4,
        2, 3, 2, 3 }; // w=2, h=2, ew=4, eh=4, offset=1
    // yf = | 2.3986    2.6004 |
    //      | 2.4080    2.5910 |

    assert(fabs(cost(eimg, 4,4,1,1,  0,0,  2,2,  1,1,0,3) - (-22.5785)) < eps);

    printf("OK !\n");
}

void test_fmincg()
{
    printf("Testing fmincg() . . . ");

    int i;

    int iState[3] = {0, 0, 0};
    float state[27];for(i=0;i<27;i++) { state[i] = 0; };
    float x[2] = {3, 5}; // initial guess
    float cost = 0;
    float grad[2] = {0, 0};

    // cost function: (x-3)^2 + (y-5)^2

    for (i = 0; i < 1000; i++) {
        fmincg(x, &cost, grad, iState, state);
        cost = (x[0]-3)*(x[0]-3) + (x[1]-5)*(x[1]-5);
        grad[0] = 2*(x[0]-3);
        grad[1] = 2*(x[1]-5);
    }
    assert(fabs(x[0] - 3) < eps);
    assert(fabs(x[1] - 5) < eps);
    printf("OK !\n");
}

void test_k_gaussian()
{
    printf("Testing k_gaussian() . . . ");

    int n = 2; // number of filters
    int n2 = 1; // for now, set to 1

    float xVec[] = { 1, 1,
        1, 1};
    int filt_size = 3;
    float theta[] = { 0,
        0 };
    float* h = malloc(n*filt_size*filt_size*sizeof(float));assert(h);


    cl_mem d_xVec = clhMalloc(context, 2*n*sizeof(float));
    cl_mem d_filter = clhMalloc(context, filt_size*filt_size* n * sizeof(float));
    cl_mem d_filter_dx = clhMalloc(context, filt_size*filt_size* n * sizeof(float));
    cl_mem d_filter_dy = clhMalloc(context, filt_size*filt_size* n * sizeof(float));
    cl_mem d_theta = clhMalloc(context, n*sizeof(float));

    clhHostToDeviceWait(command_queue, d_theta, theta, n*sizeof(float));
    clhHostToDeviceWait(command_queue, d_xVec, xVec, 2*n*sizeof(float));

    /////////////////////////////////////    Construct the filter    ////////////////////////////////////////////////////////////
    clhSetArg(k_gaussian, 0, sizeof(cl_mem), &d_xVec);
    clhSetArg(k_gaussian, 1, sizeof(cl_mem), &d_theta);
    clhSetArg(k_gaussian, 2, sizeof(cl_mem), &d_filter);
    clhSetArg(k_gaussian, 3, sizeof(cl_mem), &d_filter_dx);
    clhSetArg(k_gaussian, 4, sizeof(cl_mem), &d_filter_dy);

    clhSetArg(k_gaussian, 5, sizeof(int), &n);
    clhSetArg(k_gaussian, 6, sizeof(int), &n2);
    clhSetArg(k_gaussian, 7, sizeof(int), &filt_size);

    clhKernelCall2D(command_queue, k_gaussian, clamp2(n), clamp2(n2));
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    clhDeviceToHostWait(command_queue, d_filter, h, filt_size*filt_size* n *sizeof(float));


    //createGaussian(h, 3, 1, 1, 0);
    // h should be  | 0.0751    0.1238    0.0751 |
    //              | 0.1238    0.2042    0.1238 |
    //              | 0.0751    0.1238    0.0751 |
    //
    assert(fabs(h[4] - 0.2042) < eps);
    assert(fabs(h[0] - 0.0751) < eps);
    assert(fabs(h[7] - 0.1238) < eps);
    assert(fabs(h[8] - 0.0751) < eps);

    assert(fabs(h[filt_size*filt_size + 4] - 0.2042) < eps); // second filter should be identical to the first
    assert(fabs(h[filt_size*filt_size + 0] - 0.0751) < eps);
    assert(fabs(h[filt_size*filt_size + 7] - 0.1238) < eps);
    assert(fabs(h[filt_size*filt_size + 8] - 0.0751) < eps);

    printf("OK !\n");
}

void test_k_costimg()
{
    printf("Testing k_costimg() . . . ");

    int filt_size = 3;
    int n_w = 2;
    int n_h = 1;
    int offset = 2;
    int eblock_size = 4;
    int block_size = 2;
    int big_w = 8;
    int big_h = 6;
    int apron_size = 1;

    float eimg[] = {
        9, 3, 1, 3, 9, 3, 1, 3,
        7, 3, 8, 3, 7, 3, 8, 3,
        9, 3, 1, 3, 9, 3, 1, 3,
        7, 3, 8, 3, 7, 3, 8, 3,
        9, 3, 1, 3, 9, 3, 1, 3,
        7, 3, 8, 3, 7, 3, 8, 3  }; 

    // original image size = (3,2), 
    // | 1, 3, 9 |
    // | 8, 3, 7 |
    // block_size = 2, offset = 2, apron_size = 1 big size = (8,6)
    float h[] = {
        1, 1, 1,
        1, 1, 1,
        1, 1, 1,

        1, 1, 1,
        1, 1, 1,
        1, 1, 1 };
    float* costImg = malloc(n_w*n_h*eblock_size*eblock_size*sizeof(float)); assert(costImg);
    // y block:
    // | 3 8 3 7 | | 3 7 3 8 |
    // | 3 1 3 9 | | 3 9 3 1 |
    // | 3 8 3 7 | | 3 7 3 8 |
    // | 3 1 3 9 | | 3 9 3 1 |
    //
    // yf block:
    // | 44 28 44 43 | | 44 43 44 28 | 
    // | 49 35 49 41 | | 49 41 49 35 |
    // | 44 28 44 43 | | 44 43 44 28 | 
    // | 49 35 49 41 | | 49 41 49 35 |
    //
    // costimg:
    // | 1672 336  1672 1247 | | 1672 1247 1672 336  |
    // | 2107 1155 2107 943  | | 2107 943  2107 1155 |
    // | 1672 336  1672 1247 | | 1672 1247 1672 336  |
    // | 2107 1155 2107 943  | | 2107 943  2107 1155 |
    //
    /*
       int i, j;
       float sum = 0;
       int r=3, c=5;
       for (i = r; i < 3+r; i++) {
       for (j = c; j < c+3; j++) {
       sum += eimg[i*8 + j];
       }
       }
       printf("  %.2f  \n", sum);
       */
    
    cl_mem d_costImg = clhMalloc(context, n_w*n_h*eblock_size*eblock_size*sizeof(float));
    cl_mem d_costImg_dx = clhMalloc(context, n_w*n_h*eblock_size*eblock_size*sizeof(float));
    cl_mem d_costImg_dy = clhMalloc(context, n_w*n_h*eblock_size*eblock_size*sizeof(float));
    
    cl_mem d_eImg = clhMalloc(context, big_w*big_h*sizeof(float));

    cl_mem d_filter = clhMalloc(context, filt_size*filt_size * n_w*n_h * sizeof(float));
    cl_mem d_filter_dx = clhMalloc(context, filt_size*filt_size * n_w*n_h * sizeof(float));
    cl_mem d_filter_dy = clhMalloc(context, filt_size*filt_size * n_w*n_h * sizeof(float));


    clhHostToDeviceWait(command_queue, d_eImg, eimg, big_w*big_h*sizeof(float));
    clhHostToDeviceWait(command_queue, d_filter, h, n_w*n_h* filt_size*filt_size* sizeof(float));
    clhHostToDeviceWait(command_queue, d_filter_dx, h, n_w*n_h* filt_size*filt_size* sizeof(float));
    clhHostToDeviceWait(command_queue, d_filter_dy, h, n_w*n_h* filt_size*filt_size* sizeof(float));


    ///////////////////////////////////////////////////////////////////////////////////////////////////
    clhSetArg(k_costimg, 0, sizeof(cl_mem), &d_eImg);

    clhSetArg(k_costimg, 1, sizeof(cl_mem), &d_filter);
    clhSetArg(k_costimg, 2, sizeof(cl_mem), &d_filter_dx);
    clhSetArg(k_costimg, 3, sizeof(cl_mem), &d_filter_dy);

    clhSetArg(k_costimg, 4, sizeof(cl_mem), &d_costImg);
    clhSetArg(k_costimg, 5, sizeof(cl_mem), &d_costImg_dx);
    clhSetArg(k_costimg, 6, sizeof(cl_mem), &d_costImg_dy);

    clhSetArg(k_costimg, 7, sizeof(int), &n_w);
    clhSetArg(k_costimg, 8, sizeof(int), &n_h);
    clhSetArg(k_costimg, 9, sizeof(int), &filt_size);
    clhSetArg(k_costimg, 10, sizeof(int), &eblock_size);
    clhSetArg(k_costimg, 11, sizeof(int), &block_size);
    clhSetArg(k_costimg, 12, sizeof(int), &offset);
    clhSetArg(k_costimg, 13, sizeof(int), &big_w);
    clhSetArg(k_costimg, 14, sizeof(int), &apron_size);

    clhKernelCall2D(command_queue, k_costimg, clamp2(n_w*eblock_size), clamp2(n_h*eblock_size));
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    clhDeviceToHostWait(command_queue, d_costImg, costImg, n_w*n_h*eblock_size*eblock_size*sizeof(float));

    // costimg:
    // | 1672 336  1672 1247 | | 1672 1247 1672 336  |
    // | 2107 1155 2107 943  | | 2107 943  2107 1155 |
    // | 1672 336  1672 1247 | | 1672 1247 1672 336  |
    // | 2107 1155 2107 943  | | 2107 943  2107 1155 |

    assert(fabs(costImg[0] - 1672) < eps);
    assert(fabs(costImg[1] - 336) < eps);
    assert(fabs(costImg[4] - 1672) < eps);
    assert(fabs(costImg[7] - 336) < eps);
    assert(fabs(costImg[9] - 1155) < eps);
    assert(fabs(costImg[15] - 1155) < eps);
    assert(fabs(costImg[19] - 1247) < eps);
    assert(fabs(costImg[20] - 1672) < eps);
    assert(fabs(costImg[23] - 336) < eps);
    assert(fabs(costImg[27] - 943) < eps);
    assert(fabs(costImg[31] - 1155) < eps);

    clhDeviceToHostWait(command_queue, d_costImg_dx, costImg, n_w*n_h*eblock_size*eblock_size*sizeof(float));

    // costimg:
    // | 1672 336  1672 1247 | | 1672 1247 1672 336  |
    // | 2107 1155 2107 943  | | 2107 943  2107 1155 |
    // | 1672 336  1672 1247 | | 1672 1247 1672 336  |
    // | 2107 1155 2107 943  | | 2107 943  2107 1155 |

    assert(fabs(costImg[0] - 1672) < eps);
    assert(fabs(costImg[1] - 336) < eps);
    assert(fabs(costImg[4] - 1672) < eps);
    assert(fabs(costImg[7] - 336) < eps);
    assert(fabs(costImg[9] - 1155) < eps);
    assert(fabs(costImg[15] - 1155) < eps);
    assert(fabs(costImg[19] - 1247) < eps);
    assert(fabs(costImg[20] - 1672) < eps);
    assert(fabs(costImg[23] - 336) < eps);
    assert(fabs(costImg[27] - 943) < eps);
    assert(fabs(costImg[31] - 1155) < eps);

    clhDeviceToHostWait(command_queue, d_costImg_dy, costImg, n_w*n_h*eblock_size*eblock_size*sizeof(float));

    // costimg:
    // | 1672 336  1672 1247 | | 1672 1247 1672 336  |
    // | 2107 1155 2107 943  | | 2107 943  2107 1155 |
    // | 1672 336  1672 1247 | | 1672 1247 1672 336  |
    // | 2107 1155 2107 943  | | 2107 943  2107 1155 |

    assert(fabs(costImg[0] - 1672) < eps);
    assert(fabs(costImg[1] - 336) < eps);
    assert(fabs(costImg[4] - 1672) < eps);
    assert(fabs(costImg[7] - 336) < eps);
    assert(fabs(costImg[9] - 1155) < eps);
    assert(fabs(costImg[15] - 1155) < eps);
    assert(fabs(costImg[19] - 1247) < eps);
    assert(fabs(costImg[20] - 1672) < eps);
    assert(fabs(costImg[23] - 336) < eps);
    assert(fabs(costImg[27] - 943) < eps);
    assert(fabs(costImg[31] - 1155) < eps);

    printf("OK !\n");
}

void test_k_blockreduce()
{
    printf("Testing k_blockreduce() . . . ");

    int block_size = 4;
    int n_w = 2;
    int n_h = 1;

    float costimg[] = {
        1,1,1,1, 1,1,1,1,
        1,1,1,1, 1,1,1,1,
        1,1,3,1, 1,1,2,1,
        1,1,1,1, 1,1,1,1 }; 
    float filt[] = {
        1,1,1,
        1,2,1,
        1,1,1,

        1,1,1,
        1,1,1,
        1,1,1 };
    float sigma_im = 1;
    int filt_size = 3;
    int w = 8;
    int h = 4;

    float* cost = malloc(n_w*n_h*sizeof(float)); assert(cost);
    
    cl_mem d_costimg = clhMalloc(context, n_w*n_h*block_size*block_size*sizeof(float));
    cl_mem d_cost = clhMalloc(context, n_w*n_h*sizeof(float));
    cl_mem d_cost_dx = clhMalloc(context, n_w*n_h*sizeof(float));
    cl_mem d_cost_dy = clhMalloc(context, n_w*n_h*sizeof(float));
    cl_mem d_h = clhMalloc(context, n_w*n_h*filt_size*filt_size*sizeof(float));
    
    clhHostToDeviceWait(command_queue, d_costimg, costimg, n_w*n_h*block_size*block_size*sizeof(float));
    clhHostToDeviceWait(command_queue, d_h, filt, n_w*n_h*filt_size*filt_size*sizeof(float));
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    clhSetArg(k_blockreduce, 0, sizeof(cl_mem), &d_costimg);
    clhSetArg(k_blockreduce, 1, sizeof(cl_mem), &d_cost);

    clhSetArg(k_blockreduce, 2, sizeof(int), &w);
    clhSetArg(k_blockreduce, 3, sizeof(int), &h);
    clhSetArg(k_blockreduce, 4, sizeof(int), &n_w);
    clhSetArg(k_blockreduce, 5, sizeof(int), &n_h);
    clhSetArg(k_blockreduce, 6, sizeof(int), &block_size);

    clhSetArg(k_blockreduce, 7, sizeof(cl_mem), &d_h);
    clhSetArg(k_blockreduce, 8, sizeof(int), &filt_size);
    clhSetArg(k_blockreduce, 9, sizeof(float), &sigma_im);

    clhSetArg(k_blockreduce, 10, sizeof(cl_mem), &d_costimg);
    clhSetArg(k_blockreduce, 11, sizeof(cl_mem), &d_cost_dx);
    clhSetArg(k_blockreduce, 12, sizeof(cl_mem), &d_h);

    clhSetArg(k_blockreduce, 13, sizeof(cl_mem), &d_costimg);
    clhSetArg(k_blockreduce, 14, sizeof(cl_mem), &d_cost_dy);
    clhSetArg(k_blockreduce, 15, sizeof(cl_mem), &d_h);

    clhKernelCall2Ds(command_queue, k_blockreduce, 1, 1, n_w, n_h);
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    clhDeviceToHostWait(command_queue, d_cost, cost, n_w*n_h*sizeof(float));
    
    assert(fabs(cost[0] - 82) < eps);
    assert(fabs(cost[1] - 49) < eps);
    ///////
    clhDeviceToHostWait(command_queue, d_cost_dx, cost, n_w*n_h*sizeof(float));
    
    assert(fabs(cost[0] - 0) < eps);
    assert(fabs(cost[1] - 0) < eps);
    ///////////
    clhDeviceToHostWait(command_queue, d_cost_dy, cost, n_w*n_h*sizeof(float));
    
    assert(fabs(cost[0] - 0) < eps);
    assert(fabs(cost[1] - 0) < eps);


    printf("OK !\n");
}
