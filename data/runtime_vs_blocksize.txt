; All tests were conducted on a Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz with 8 GB of RAM
; and AMD Radeon HD 7950 at 850MHz (Up to 925MHz with boost) with 3 GB GDDR5 running up to 1250MHz (5.0 Gbps GDDR5)
; The OS used was Ubuntu 12.04 x64 running Linux 3.8.0-29-generic x86_64
; All times reported are wall clock times
; For the CPU version, 4 threads were run in parallel
; In the GPU version, convolution is done with pixel level parallelism, but summation and fmincg are done with block level parallelism

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; filter-size=9x9, Lena (512x512) 8-bit greyscale, Input noise level = 20 (input PSNR=22.14dB), apron_size=0
; Block-Size CPU-Runtime(s) GPU-Runtime(s)

512x512   50.56  55.31
256x256   28.94  14.17
128x128   21.89   3.96
  64x64   16.92   1.45
  32x32   14.86   0.82
  16x16   14.34   0.77
    8x8   17.09   1.04
    4x4   30.95   2.63

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; filter-size=9x9, Lena (512x512) 8-bit greyscale, Input noise level = 20 (input PSNR=22.14dB), apron_size=4
; Block-Size CPU-Runtime(s)  GPU-Runtime(s)

512x512   54.64  57.79
256x256   30.78  15.15
128x128   24.68   4.39
  64x64   21.16   1.71
  32x32   22.45   1.01
  16x16   30.36   1.05
    8x8   53.25   1.70
    4x4  476.54   4.35

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; filter-size=9x9, Lena (512x512) 8-bit greyscale, Input noise level = 20 (input PSNR=22.14dB), apron_size=8
; Block-Size CPU-Rumtime(s)  GPU-Runtime(s)

512x512   55.14  58.82
256x256   33.36  15.99
128x128   27.61   4.91
  64x64   25.48   1.99
  32x32   31.94   1.24
  16x16   51.70   1.52
    8x8  111.91   2.93
    4x4 3064.11   8.20

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
