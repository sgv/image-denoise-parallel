image-denoise-parallel
======================

This research project investigates how well brute force image denoising with only guassian filters performs.

- `reference/` has a reference implementation in C.
- `GPU/` has an implementation that uses GPU for speed-up.

Dependencies
------------

- OpenCV (I used version 2.4.8)
- OpenCL SDK (only for the GPU version)
- Linux (I used Ubuntu 12.04 running Linux 3.8.0-29 x86_64)

How-to-use
----------

- Open Makefile and modify the OpenCL path depending on what SDK you use (for the GPU version)
- Run `make` in `reference/` or `GPU/`
- If all went well, executable `imgdenoise` should be created
- To denoise an image, run `./imgdenoise noise_image.png -o cleaned_image.png`
- If you want to directly see the output, run `./imgdenoise noisy_image.png -s` 
- Use multiple verbose flags increase details in the console log `./imgdenoise noisy_img.png -vvvs`
- Run `./imgdenoise --help` to view command line options
