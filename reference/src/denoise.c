/*
The MIT License (MIT)

Copyright (c) 2014 Sagar G V (sagar.writeme@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include "denoise.h"
#include <omp.h>

extern int verbosity;
float mirror(float* img, int w, int h, int x, int y); // returns pixel (x,y) with mirror boundary applied.
float getepx(float* eimg, int ew, int eh, int offset, int x, int y); 
struct timespec diff(struct timespec start, struct timespec end);

//#define DEBUG_NUMERICAL_ACCURACY

void denoise(float* img, int w, int h, int block_size, int filter_size, int apron_size, float* imgOut)
{
    assert(filter_size%2 == 1);
    assert(filter_size >= 3);

    struct timespec t1, t2, t3, t4, t5, t6;

    int new_w, new_h, offset, x, y, big_w, big_h, i, j;
    int n_w, n_h;

    new_w = w + block_size - (w % block_size);
    new_h = h + block_size - (h % block_size);
    
    n_w = new_w / block_size;
    n_h = new_h / block_size;

    float *orient = malloc(n_w * n_h * sizeof(float));
    float *sigmax = malloc(n_w * n_h * sizeof(float));
    float *sigmay = malloc(n_w * n_h * sizeof(float));

    offset = filter_size/2 + apron_size;
    
    big_w = new_w + 2*offset;
    big_h = new_h + 2*offset;

    float *eImg = malloc( (new_w + 2*offset) * (new_h + 2*offset) * sizeof(float) );
    assert(eImg != NULL);
    
    // expand into big image (using mirror boundary conditions)
    imgExpand(img, w, h, new_w, new_h, offset, eImg);

    // get estimate of noise level in the image
    float sigma_est = estimNoise(eImg, big_w, big_h, w, h, offset);
    if(verbosity >= 3)
        printf("Estimated noise standard deviation = %.2f\n", sigma_est);
    
    // get orientation of each block using the structure tensor
    assert(orient != NULL);
    for (y = 0; y < n_h; y++) {
        for (x = 0; x < n_w; x++) {
            orient[x + y*n_w] = stTensorOrient(eImg, big_w, big_h, offset,
                                                x*block_size-apron_size, y*block_size-apron_size, 
                                                (x+1)*block_size+apron_size, (y+1)*block_size+apron_size);
        }
    }

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
    clock_gettime(CLOCK_REALTIME, &t3);
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &t5);
    double ot1 = omp_get_wtime();

    // find optimal sigmax and sigmay for each block
    float *xVec = malloc(2*n_w*n_h*sizeof(float)); assert(xVec);
    float *costVal = malloc(n_w*n_h*sizeof(float)); assert(costVal);
    float *gradCost = malloc(2*n_w*n_h*sizeof(float)); assert(gradCost);
    int *iState = malloc(3*n_w*n_h*sizeof(int)); assert(iState);
    float *state = malloc(27*n_w*n_h*sizeof(float)); assert(state);

    for (y = 0; y < n_h; y++) {
        for (x = 0; x < n_w; x++) {
            xVec[2*(x + y*n_w)] = 1.0; // initial guess
            xVec[2*(x + y*n_w) + 1] = 1.0;
            
            costVal[x + y*n_w] = 0;

            gradCost[2*(x + y*n_w)] = 0;
            gradCost[2*(x + y*n_w) + 1] = 0;
                        
            for (i = 0; i < 3; i++)
                iState[3*(x + y*n_w) + i] = 0;
            for (i = 0; i < 27; i++) 
                state[27*(x + y*n_w) + i] = 0;
            
        }
    }
    // run optimization algorithm
    for(i=0;i < 200; i++) {
        for(y = 0; y < n_h; y++) {
            for(x = 0; x < n_w; x++) {
                fmincg(&xVec[2*(x + y*n_w)], &costVal[x + y*n_w], &gradCost[2*(x + y*n_w)], &iState[3*(x + y*n_w)], &state[27*(x + y*n_w)]);
            }
        }
#ifdef DEBUG_NUMERICAL_ACCURACY
        int dbg_n = 1;
        xVec[dbg_n*2] = xVec[dbg_n*2 + 1] = 5;
#endif

        #pragma omp parallel for num_threads(4) private(x)
        for(y = 0; y < n_h; y++) {
            for(x = 0; x < n_w; x++) {
                costVal[x + y*n_w] = 
                            cost(eImg, big_w, big_h, offset, sigma_est,
                            x*block_size-apron_size, y*block_size-apron_size, (x+1)*block_size+apron_size, (y+1)*block_size+apron_size,
                            xVec[2*(x + y*n_w)], xVec[2*(x + y*n_w)+1], orient[x + y*n_w], filter_size);
#ifdef DEBUG_NUMERICAL_ACCURACY
                float *h = malloc(filter_size*filter_size*sizeof(float)); assert(h);
                createGaussian(h, filter_size, xVec[2*(x + y*n_w)], xVec[2*(x + y*n_w)+1], orient[x + y*n_w]);
                //costVal[x + y*n_w] = h[filter_size*filter_size - 1];//filter_size*(filter_size/2) + (filter_size/2)];
#endif
                gradCost[2*(x + y*n_w)] = costderivx(eImg, big_w, big_h, offset, sigma_est,
                            x*block_size-apron_size, y*block_size-apron_size, (x+1)*block_size+apron_size, (y+1)*block_size+apron_size,
                            xVec[2*(x + y*n_w)], xVec[2*(x + y*n_w)+1], orient[x + y*n_w], filter_size);

                            //(cost(eImg, big_w, big_h, offset, sigma_est,
                            //x*block_size-apron_size, y*block_size-apron_size, (x+1)*block_size+apron_size, (y+1)*block_size+apron_size,
                            //xVec[2*(x + y*n_w)]+DS, xVec[2*(x + y*n_w)+1], orient[x + y*n_w], filter_size) - costVal[x + y*n_w])/DS;

                gradCost[2*(x + y*n_w) + 1] = costderivy(eImg, big_w, big_h, offset, sigma_est,
                            x*block_size-apron_size, y*block_size-apron_size, (x+1)*block_size+apron_size, (y+1)*block_size+apron_size,
                            xVec[2*(x + y*n_w)], xVec[2*(x + y*n_w)+1], orient[x + y*n_w], filter_size);

                            //(cost(eImg, big_w, big_h, offset, sigma_est,
                            //x*block_size-apron_size, y*block_size-apron_size, (x+1)*block_size+apron_size, (y+1)*block_size+apron_size,
                            //xVec[2*(x + y*n_w)], xVec[2*(x + y*n_w)+1]+DS, orient[x + y*n_w], filter_size) - costVal[x + y*n_w])/DS;

            }
        }
#ifdef DEBUG_NUMERICAL_ACCURACY
        printf("sigma-x: %.12f, sigma-y: %.12f\n", xVec[dbg_n*2], xVec[dbg_n*2 + 1]);
        printf("Cost: %.12f\n", costVal[dbg_n]);
        printf("grad-x: %.12f, grad-y: %.12f\n", gradCost[dbg_n*2], gradCost[dbg_n*2 + 1]);
#endif


    }

    for (y = 0; y < n_h; y++) {
        for (x = 0; x < n_w; x++) {
            sigmax[x + y*n_w] = xVec[2*(x + y*n_w)];
            sigmay[x + y*n_w] = xVec[2*(x + y*n_w) + 1];
        }
    }
    free(xVec); free(costVal); free(gradCost); free(iState); free(state);

    
    // filter the image with the optimal filters
    float *optfilt = malloc(filter_size*filter_size * sizeof(float));assert(optfilt);
    for (j = 0; j < n_h; j++) {
        for (i = 0; i < n_w; i++) {
            createGaussian(optfilt, filter_size, sigmax[i + j*n_w], sigmay[i + j*n_w], orient[i + j*n_w]);
            for (y = j*block_size; y < (j+1)*block_size; y++) {
                for (x = i*block_size; x < (i+1)*block_size; x++) {
                    if(x < w && y < h) {
                        imgOut[x + y*w] = conv(eImg, big_w, big_h, offset, x, y, optfilt, filter_size);//eImg[(x+offset) + (y+offset)*big_w];
                    }
                }
            }
        }
    }
    free(optfilt);

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);
    clock_gettime(CLOCK_REALTIME, &t4);
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &t6);
    double ot2 = omp_get_wtime();

    struct timespec dt = diff(t1, t2);
    struct timespec dt2 = diff(t3, t4);
    struct timespec dt3 = diff(t5, t6);
    
    if(verbosity >= 2) {
        printf("Denoising time (OMP wall clock time) = %.2f ms\n", (ot2-ot1)*1000);
    }
    if(verbosity >= 3) {
        printf("Denoising time (CPU Process time) = %.2f ms\n", dt.tv_sec * 1000 + (dt.tv_nsec / 1e6));
        printf("Denoising time (CPU Real time) = %.2f ms\n", dt2.tv_sec * 1000 + (dt2.tv_nsec / 1e6));
        printf("Denoising time (CPU thread time) = %.2f ms\n", dt3.tv_sec * 1000 + (dt3.tv_nsec / 1e6));
    }

    free(eImg);
    free(orient);
    free(sigmax);
    free(sigmay);
}
float costderivx(float *eimg, int ew, int eh, int offset, float sigma_est,
        int xstart, int ystart, int xstop, int ystop,
        float sigmax, float sigmay, float theta, int filt_size)
{
    float *hx = malloc(filt_size * filt_size * sizeof(float));assert(hx);
    float *h = malloc(filt_size * filt_size * sizeof(float));assert(h);
    createGaussian(h, filt_size, sigmax, sigmay, theta);

    int x,y;
    float acc, accdx, val, xtheta, ytheta;
    acc = accdx = 0;
    for (y = -filt_size/2; y <= filt_size/2; y++) 
    {
        for (x = -filt_size/2; x <= filt_size/2; x++) 
        {
            xtheta = x*cos(theta) + y*sin(theta);
			ytheta = -x*sin(theta) + y*cos(theta);

            val = exp(-0.5*( (xtheta*xtheta)/(sigmax*sigmax) + (ytheta*ytheta)/(sigmay*sigmay) ));
            hx[(x + filt_size/2) + (y + filt_size/2)*filt_size] = val;
            acc += val;
            accdx += (xtheta*xtheta)/(sigmax*sigmax*sigmax) * val;
        }    
    }
    for (y = -filt_size/2; y <= filt_size/2; y++) 
    {
        for (x = -filt_size/2; x <= filt_size/2; x++) 
        {
            xtheta = x*cos(theta) + y*sin(theta);
			ytheta = -x*sin(theta) + y*cos(theta);
            
            val = hx[(x + filt_size/2) + (y + filt_size/2)*filt_size];
            hx[(x + filt_size/2) + (y + filt_size/2)*filt_size] = val * (acc * (xtheta*xtheta)/(sigmax*sigmax*sigmax) - accdx)/(acc*acc);
        }    
    }

    float cost = 0;
    float filt, filtd, samp;

    for(y = ystart; y < ystop; y++) 
    {
        for(x = xstart; x < xstop; x++)
        {
            samp = eimg[ (x+offset) + (y+offset)*ew ];
            filt = conv(eimg, ew, eh, offset, x, y, h, filt_size);
            filtd = conv(eimg, ew, eh, offset, x, y, hx, filt_size);
            cost += 2*filtd*(filt - samp);
        }
    }
    cost += 2*(sigma_est*sigma_est)*((xstop-xstart)*(ystop-ystart))*hx[filt_size/2 + (filt_size/2)*filt_size];
    
    free(hx);free(h);

    return cost;
}
float costderivy(float *eimg, int ew, int eh, int offset, float sigma_est,
        int xstart, int ystart, int xstop, int ystop,
        float sigmax, float sigmay, float theta, int filt_size)
{
    float *hy = malloc(filt_size * filt_size * sizeof(float));assert(hy);
    float *h = malloc(filt_size * filt_size * sizeof(float));assert(h);
    createGaussian(h, filt_size, sigmax, sigmay, theta);

    int x,y;
    float acc, accdy, val, xtheta, ytheta;
    acc = accdy = 0;
    for (y = -filt_size/2; y <= filt_size/2; y++) 
    {
        for (x = -filt_size/2; x <= filt_size/2; x++) 
        {
            xtheta = x*cos(theta) + y*sin(theta);
			ytheta = -x*sin(theta) + y*cos(theta);

            val = exp(-0.5*( (xtheta*xtheta)/(sigmax*sigmax) + (ytheta*ytheta)/(sigmay*sigmay) ));
            hy[(x + filt_size/2) + (y + filt_size/2)*filt_size] = val;
            acc += val;
            accdy += (ytheta*ytheta)/(sigmay*sigmay*sigmay) * val;
        }    
    }
    for (y = -filt_size/2; y <= filt_size/2; y++) 
    {
        for (x = -filt_size/2; x <= filt_size/2; x++) 
        {
            xtheta = x*cos(theta) + y*sin(theta);
			ytheta = -x*sin(theta) + y*cos(theta);
            
            val = hy[(x + filt_size/2) + (y + filt_size/2)*filt_size];
            hy[(x + filt_size/2) + (y + filt_size/2)*filt_size] = val * (acc * (ytheta*ytheta)/(sigmay*sigmay*sigmay) - accdy)/(acc*acc);
        }    
    }

    float cost = 0;
    float filt, filtd, samp;

    for(y = ystart; y < ystop; y++) 
    {
        for(x = xstart; x < xstop; x++)
        {
            samp = eimg[ (x+offset) + (y+offset)*ew ];
            filt = conv(eimg, ew, eh, offset, x, y, h, filt_size);
            filtd = conv(eimg, ew, eh, offset, x, y, hy, filt_size);
            cost += 2*filtd*(filt - samp);
        }
    }
    cost += 2*(sigma_est*sigma_est)*((xstop-xstart)*(ystop-ystart))*hy[filt_size/2 + (filt_size/2)*filt_size];
    
    free(hy);free(h);

    return cost;
}

float cost(float* eimg, int ew, int eh, int offset, float sigma_est, 
        int xstart, int ystart, int xstop, int ystop,
        float sigmax, float sigmay, float theta, int filt_size)
{
    float *h = malloc(filt_size * filt_size * sizeof(float));
    assert(h);
    createGaussian(h, filt_size, sigmax, sigmay, theta);

    int x, y;
    float cost = 0;
    float filt, samp;

    for(y = ystart; y < ystop; y++) 
    {
        for(x = xstart; x < xstop; x++)
        {
            samp = eimg[ (x+offset) + (y+offset)*ew ];
            filt = conv(eimg, ew, eh, offset, x, y, h, filt_size);
            cost += filt*(filt - 2*samp);
        }
    }
    cost += 2*(sigma_est*sigma_est)*((xstop-xstart)*(ystop-ystart))*h[filt_size/2 + (filt_size/2)*filt_size];
    free(h);
    return cost;
}
void createGaussian(float *h, int filt_size, float sigmax, float sigmay, float theta)
{
    assert(filt_size % 2 == 1);

    int x, y;
    float acc = 0;
    float value, x_theta, y_theta;

    for (int y = -filt_size / 2; y <= filt_size / 2; y++) {
	    for (int x = -filt_size / 2; x <= filt_size / 2; x++) {
		    x_theta = x*cos(theta) + y*sin(theta);
			y_theta = -x*sin(theta) + y*cos(theta);
		    
			value = exp(-0.5*((x_theta * x_theta) / (sigmax * sigmax) + (y_theta * y_theta) / (sigmay * sigmay)));
			acc += value;
			h[ (x + filt_size / 2) + (y + filt_size / 2) * filt_size ] = value;
		}
	}
    for (y = -filt_size/2; y <= filt_size/2; y++) {
        for (x = -filt_size/2; x <= filt_size/2; x++) {
            h[ (x + filt_size / 2) + (y + filt_size / 2) * filt_size ] /= acc;
        }
    }
}
float stTensorOrient(float *eimg, int ew, int eh, int offset, int xstart, int ystart, int xstop, int ystop) 
{
    float acc_xx, acc_xy, acc_yy;
    float dx, dy;
    int x, y;

    float hy[] = {
        -3, -10, -3,
         0,   0,  0,
         3,  10,  3 };
    float hx[] = {
         -3,  0,  3,
        -10,  0, 10,
         -3,  0,  3 };

    acc_xx = 0;
    acc_xy = 0;
    acc_yy = 0;

    for (y = ystart; y < ystop; y++) {
        for (x = xstart; x < xstop; x++) {
            dx = conv(eimg, ew, eh, offset, x, y, hx, 3);
            dy = conv(eimg, ew, eh, offset, x, y, hy, 3);
            acc_xx += dx * dx;
            acc_yy += dy * dy;
            acc_xy += dx * dy;
        }
    }
    return M_PI_2 + 0.5*atan(2.0f * acc_xy / (acc_yy - acc_xx));
}
void identity(float* img, int w, int h, float* imgOut) 
{
    int x, y;
    for (y = 0; y < h; y++) 
    {
        for (x = 0; x < w; x++) 
        {
            imgOut[x + y*w] = img[x + y*w];
        }
    }

}
float mirror(float* img, int w, int h, int x, int y) 
{
    if(x < 0)
        return mirror(img, w, h, -x, y);
    if(x >= w)
        return mirror(img, w, h, 2*w - x - 2, y);
    if(y < 0)
        return mirror(img, w, h, x, -y);
    if(y >= h)
        return mirror(img, w, h, x, 2*h - y - 2);
    return img[x + y*w];
}
float getepx(float* eimg, int ew, int eh, int offset, int x, int y) 
{
    return eimg[ (x+offset) + (y+offset) * ew];
}
void imgExpand(float* img, int w, int h, int new_w, int new_h, int offset, float* expandedImg) 
{
    int x, y;
    for (y = -offset; y < new_h+offset; y++) {
        for (x = -offset; x < new_w+offset; x++) {
            expandedImg[(x+offset) + (y+offset)*(new_w+2*offset)] = mirror(img, w, h, x, y);
        }
    }
}
float estimNoise(float* eimg, int ew, int eh, int w, int h, int offset)
{
    float nfilt[] = {
        -1.0f/9, -1.0f/9, -1.0f/9,
        -1.0f/9, +8.0f/9, -1.0f/9,
        -1.0f/9, -1.0f/9, -1.0f/9,
    };
    float *nin = malloc(w*h*sizeof(float));
    assert(nin != NULL);

    int x, y;
    float med;

    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++) {
            nin[x + y*w] = fabs(conv(eimg, ew, eh, offset, x, y, nfilt, 3));
        }
    }
    med = median(nin, w*h);
    free(nin);
    return med/0.6475;
}
float conv(float *eimg, int ew, int eh, int offset, int x, int y, float *filter, int filt_size)
{
    assert(filt_size % 2 == 1);

    int i, j;
    float acc = 0;
    for(j = -filt_size/2; j <= filt_size/2; j++)
    {
        for(i = -filt_size/2; i <= filt_size/2; i++)
        {
            acc += eimg[ (x - i + offset) + (y - j + offset)*ew  ] * filter[ (i+filt_size/2) + (j+filt_size/2)*filt_size ];
        }
    }
    
    return acc;
}
struct timespec diff(struct timespec start, struct timespec end)
{
	struct timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}
