/*
The MIT License (MIT)

Copyright (c) 2014 Sagar G V (sagar.writeme@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#include "imgdenoise.h"
#include "denoise.h"
#include "gopt.h"

int verbosity;

int main(int argc, const char *argv[])
{
    int displayImgs;
    int block_size, apron_size, filter_size;

    const char* noisy_file_name;
    char* denoised_file_name;

    IplImage *noisyImage, *denoisedImage, *noisyFloatImage, *denoisedFloatImage;
    float *noiseFloat, *denoisedFloat;

    int width, height;

    readCommandLine(&argc, argv, &noisy_file_name, &denoised_file_name, &verbosity, &displayImgs, &block_size,  
            &apron_size, &filter_size);

    if(verbosity >= 2) {
        printf("Filter-size: %d\n", filter_size);
        printf("Apron-size: %d\n", apron_size);
        printf("Block-size: %d\n", block_size);
    }

    if(verbosity >= 1) {
        printf("Opening file %s for denoising...\n",noisy_file_name);
    }
    noisyImage = cvLoadImage(noisy_file_name, CV_LOAD_IMAGE_GRAYSCALE);
    if(!noisyImage) {
        fprintf(stderr, "Error opening file %s\n",noisy_file_name);
        exit(5);
    }

    width = noisyImage->width;
    height = noisyImage->height;

    // Allocate memory for images
    noisyFloatImage = cvCreateImage( cvSize(width,height), IPL_DEPTH_32F, 1);
    denoisedFloatImage = cvCreateImage( cvSize(width,height), IPL_DEPTH_32F, 1);
    denoisedImage = cvCreateImage( cvSize(width, height), noisyImage->depth, 1);
    if(!denoisedImage || !denoisedFloatImage || !noisyFloatImage) {
        fprintf(stderr, "Out of memory\n");
        exit(1);
    }

    if(verbosity >= 2) {
        printf("Converting image pixels to float type...\n");
    }
    cvConvertScale(noisyImage, noisyFloatImage, 1, 0);

    noiseFloat = (float *)noisyFloatImage->imageData;
    denoisedFloat = (float *)denoisedFloatImage->imageData;
    
    
    if(verbosity >= 1) {
        printf("Denoising image...\n");
    }
    
    denoise(noiseFloat, width, height, block_size, filter_size, apron_size, denoisedFloat); 
    

    if(verbosity >= 2) {
        printf("Converting denoised float array to image\n");
    }
    cvConvertScale(denoisedFloatImage, denoisedImage, 1, 0);

    if(verbosity >= 1) {
        printf("Writing denoised image to %s...\n",denoised_file_name);
    }
    if(!cvSaveImage(denoised_file_name, denoisedImage, 0)) {
        fprintf(stderr, "Could not save: %s\n", denoised_file_name); 
        exit(5);
    }

    if(displayImgs) {
        if(verbosity >= 1) {
            printf("Displaying the original (noisy) and the denoised image. Press any key to close...\n");
        }
        // display both images
        cvNamedWindow("original", CV_WINDOW_AUTOSIZE); 
        cvMoveWindow("original", 100, 100);
        cvShowImage("original",noisyImage);

        cvNamedWindow("denoised", CV_WINDOW_AUTOSIZE); 
        cvMoveWindow("denoised", 900, 100);        
        cvShowImage("denoised",denoisedImage);

        cvWaitKey(0);
    }
    if(verbosity >= 2) {
        printf("Freeing memory and exiting...\n");
    }
    cvReleaseImage(&noisyImage);
    cvReleaseImage(&denoisedImage);
    cvReleaseImage(&noisyFloatImage);
    cvReleaseImage(&denoisedFloatImage);

    return 0;
}

void readCommandLine(int *argc_ptr, const char *argv[], const char** noisy_file_name, char** denoised_file_name, int *verbosity, int *displayImgs, int *block_size, int* apron_size, int* filter_size) {

    const char *temp;
    int i;
    *verbosity = 0;
    *displayImgs = 0;
    *block_size = 8;
    *apron_size = 0;
    *filter_size = 7;

    int default_file_name = 1; // append "denoise_" to input argument
    // read command line options
    void *options = gopt_sort( argc_ptr, argv, gopt_start(
                gopt_option( 'h', 0, gopt_shorts( 'h', '?' ), gopt_longs( "help", "HELP" )),
                gopt_option( 's', 0, gopt_shorts( 's' ), gopt_longs( "show", "SHOW" )),
                gopt_option( 'v', GOPT_REPEAT, gopt_shorts( 'v' ), gopt_longs( "verbose" )),
                gopt_option( 'o', GOPT_ARG, gopt_shorts( 'o' ), gopt_longs( "output" )),
                gopt_option( 'b', GOPT_ARG, gopt_shorts( 'b' ), gopt_longs( "blocklevel" )),
                gopt_option( 'f', GOPT_ARG, gopt_shorts( 'f' ), gopt_longs( "filtersize" )),
                gopt_option( 'a', GOPT_ARG, gopt_shorts( 'a' ), gopt_longs( "apronsize" ))
                ));

    *verbosity = gopt( options, 'v' );
    *displayImgs = gopt(options, 's');
    *apron_size = gopt(options, 'a');

    if(gopt_arg(options, 'f', &temp)) {
        *filter_size = atoi(temp);
    }
    if(gopt_arg(options, 'a', &temp)) {
        *apron_size = atoi(temp);
    }
    if(gopt_arg(options, 'b', &temp)) {
        *block_size = atoi(temp);
    }

    if(gopt(options,'h')) {
        printUsage();
        gopt_free( options );
        exit(0);
    }
    if( gopt_arg(options, 'o', (const char **) denoised_file_name ) && strcmp( *denoised_file_name, "-") ) {
        default_file_name = 0;
    }
    gopt_free( options );
    if(*argc_ptr != 2) {
        printf("Invalid number of arguments.\n\n");
        printUsage();
        exit(1);
    }
    *noisy_file_name = argv[1];
    if(default_file_name) {
        *denoised_file_name = malloc(MAX_FILENAME_CHARS * sizeof(char));
        if(*denoised_file_name == NULL) {
            fprintf(stderr, "Out of memory !\n");
            exit(2);
        }
        strncpy(*denoised_file_name, *noisy_file_name, MAX_FILENAME_CHARS);
        for(i=strlen(*denoised_file_name)-1; i > 0; i--) {
            if((*denoised_file_name)[i] == '.') {
                break;
            }
        }
        if(i == 0) {
            i = strlen(*denoised_file_name);
        }
        strcpy(*denoised_file_name + i, "_denoised");
        strncpy(*denoised_file_name + strlen(*denoised_file_name), *noisy_file_name + i, MAX_FILENAME_CHARS - strlen(*denoised_file_name)-2);
    }

}

void printUsage() {
    printf("Usage: imgdenoise [options] noisy_img_file\n");
    printf("Denoise images with gaussian filter\n\n");
    printf("Options:\n");
    printf("\t-o <file>        Place denoised image into <file>\n");
    printf("\t-b <number>      Force block size to <number>. Default is 8 px\n");
    printf("\t-f <number>      Set filter_size to <number>. Default is 7\n");
    printf("\t-a <number>      Set apron_size to <number>. Default is 0\n");
    printf("\t-s, --show       Display the noisy and denoised images\n");
    printf("\t-v, --verbose    Verbose output. Use this multiple times to increase verbosity\n");
    printf("\t-h, --help       Display this message and exit\n");
}

