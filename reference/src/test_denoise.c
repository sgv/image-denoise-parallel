/*
The MIT License (MIT)

Copyright (c) 2014 Sagar G V (sagar.writeme@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "denoise.h"
#include "fmincg.h"

void test_mirror();
void test_imgExpand();
void test_conv();
void test_estimNoise();
void test_stTensorOrient();
void test_createGaussian();
void test_cost();
void test_fmincg();

float eps = 1e-3;

float mirror(float* img, int w, int h, int x, int y);

#define DEBUGF(x) printf("\nDEBUG: %.4f\n", x);

int verbosity=0;

int main()
{
    test_mirror();
    test_imgExpand();
    test_conv();
    test_estimNoise();
    test_stTensorOrient();
    test_createGaussian();
    test_cost();
    test_fmincg();
    return 0;
}

void test_mirror()
{
    printf("Testing mirror() . . . ");
    // For this 2x2 matrix, mirror(-1,0) == 3, and mirror(3,2) == 3
    float arr[] = { 1, 3, 9,
                    8, 3, 7 };
    assert(mirror(arr, 3, 2, -1, 0) == 3);
    assert(mirror(arr, 3, 2, 3, 2) == 3);
    printf("OK !\n");
}
void test_imgExpand() 
{
    printf("Testing imgExpand() . . . ");
    float arr[] = { 1, 3, 9,
                    8, 3, 7 };
    // For this 2x2 matrix, with 
    // imgExpand with new_width=4, new_height=2, offset=1,
    // we should be getting
    // | 3 8 3 7 3 8 |
    // | 3 1 3 9 3 1 |
    // | 3 8 3 7 3 8 |
    // | 3 1 3 9 3 1 |
    float* earr = malloc((4+2)*(2+2)*sizeof(float));
    assert(earr != 0);

    imgExpand(arr, 3, 2, 4, 2, 1, earr);

    assert(earr[0] == 3);
    assert(earr[5] == 8);
    assert(earr[6] == 3);
    assert(earr[1] == 8);
    assert(earr[7] == 1);
    assert(earr[23] == 1);

    printf("OK !\n");
}

void test_conv() 
{
    printf("Testing conv() . . . ");
    float eimg[] = {
        3, 8, 3, 7, 3, 8,
        3, 1, 3, 9, 3, 1,
        3, 8, 3, 7, 3, 8,
        3, 1, 3, 9, 3, 1  }; // image (6,4) with offset = 1
    float mask[] = {
        1, 1, 1,
        1, 1, -1,
        1, 1, 1  };
    
    assert(fabs(conv(eimg, 6, 4, 1, 2, 1, mask, 3) - 37) < eps);
    assert(fabs(conv(eimg, 6, 4, 1, 0, 0, mask, 3) - 29) < eps);

    printf("OK !\n");
}

void test_estimNoise()
{
    printf("Testing estimNoise() . . . ");
    // original
    // |3 2|
    // |4 1|
    float eimg[] = {
        1, 4, 1, 4,
        2, 3, 2, 3,
        1, 4, 1, 4,
        2, 3, 2, 3 }; // w=2, h=2, ew=4, eh=4, offset=1
    // After filtering
    // (1/9) * | 8  8  |
    //         | 12 16 |
    // After absing
    // { (8/9), (8/9), (16/9), (16/9) }
    // Median = (8/9)
    // Noise estimate = (8/9) / 0.675 = 1.37280137
    assert(fabs(estimNoise(eimg, 4, 4, 2, 2, 1) - 1.37280137) < eps);

    printf("OK !\n");
}

void test_stTensorOrient()
{
    printf("Testing stTensorOrient() . . . ");
    float eimg[] = {
        3, 8, 3, 7, 3, 8,
        3, 1, 3, 9, 3, 1,
        3, 8, 3, 7, 3, 8,
        3, 1, 3, 9, 3, 1  }; // original image size = (3,2), block_size = 2, offset = 1. big size = (6,4)

    // x-deriv = | 0  74  0 |
    //           | 0  38  0 | 
    //
    // y-deriv = | 0   0  0 |
    //           | 0   0  0 |
    // xx = 74^2 + 38^2
    // xy = 0
    // yy = 0
    // Orient = PI/2 + 0.5*atan(2 * 0 / (xx - yy)) = PI/2
    assert(fabs(stTensorOrient(eimg, 6, 4, 1, 0, 0, 3, 2) - 1.5707963267948966) < eps);

    printf("OK !\n");
}

void test_createGaussian()
{
    printf("Testing createGaussian() . . . ");
    float h[9];
    createGaussian(h, 3, 1, 1, 0);
    // h should be  | 0.0751    0.1238    0.0751 |
    //              | 0.1238    0.2042    0.1238 |
    //              | 0.0751    0.1238    0.0751 |
    //
    assert(fabs(h[4] - 0.2042) < eps);
    assert(fabs(h[0] - 0.0751) < eps);
    assert(fabs(h[7] - 0.1238) < eps);
    assert(fabs(h[8] - 0.0751) < eps);
    printf("OK !\n");
}
void test_cost()
{
    printf("Testing cost() . . . ");
    // original
    // y = |3 2|
    //     |4 1|
    float eimg[] = {
        1, 4, 1, 4,
        2, 3, 2, 3,
        1, 4, 1, 4,
        2, 3, 2, 3 }; // w=2, h=2, ew=4, eh=4, offset=1
    // yf = | 2.3986    2.6004 |
    //      | 2.4080    2.5910 |

    assert(fabs(cost(eimg, 4,4,1,1,  0,0,  2,2,  1,1,0,3) - (-22.5785)) < eps);

    printf("OK !\n");
}

void test_fmincg()
{
    printf("Testing fmincg() . . . ");

    int i;

    int iState[3] = {0, 0, 0};
    float state[27];for(i=0;i<27;i++) { state[i] = 0; };
    float x[2] = {3, 5}; // initial guess
    float cost = 0;
    float grad[2] = {0, 0};
    
    // cost function: (x-3)^2 + (y-5)^2

    for (i = 0; i < 1000; i++) {
        fmincg(x, &cost, grad, iState, state);
        cost = (x[0]-3)*(x[0]-3) + (x[1]-5)*(x[1]-5);
        grad[0] = 2*(x[0]-3);
        grad[1] = 2*(x[1]-5);
    }
    assert(fabs(x[0] - 3) < eps);
    assert(fabs(x[1] - 5) < eps);
    printf("OK !\n");
}
