/*
The MIT License (MIT)

Copyright (c) 2014 Sagar G V (sagar.writeme@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __DENOISE_H__
#define __DENOISE_H__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <time.h>

#include "median.h"
#include "fmincg.h"

// All image inputs are assumed to be in row-major order. 
// i.e.. with (x,y) co-ordinates location [0] -> (0,0), [1] -> (1,0) . . . [w] -> (0,1) and so on

// reference implementation of SVOGS. memory for imgOut has to be allocated by the caller.
void denoise(float* img, int w, int h, int block_size, int filter_size, int apron_size, float* imgOut);

// copies 2D array contents img to imgOut. memory for imgOut has to be allocated by the caller.
void identity(float* img, int w, int h, float* imgOut); 

// expands the image with mirror boundary condition. 'w' and 'h' are the width and height of img.
// 'offset' is the amount by which the image is expanded on all sides.
// memory for 'expandedImg' should be allocated by caller. i.e.. (new_w+2*offset)*(new_h+2*offset) should be allocated.
void imgExpand(float* img, int w, int h, int new_w, int new_h, int offset, float* expandedImg);

// *eimg is the extended image with with bigwidth ew and offset 'offset'. This function returns in the result of colvolution at (x,y)
float conv(float *emig, int ew, int eh, int offset, int x, int y, float *filter, int filt_size);

// estimate noise in the image. Noise is assumed to be white.
float estimNoise(float* eimg, int ew, int eh, int w, int h, int offset);

// compute orientation of extended image eimg using structure tensor.
// orientation is computed for the region (xstart, ystart) -> (xstop, ystop). 
// Note that (xstop, ystop) is not included but (xstart, ystart) is.
// the resulted is returned in radians.
float stTensorOrient(float *eimg, int ew, int eh, int offset, int xstart, int ystart, int xstop, int ystop); 

// memory for h should be allocated by the caller
void createGaussian(float *h, int filt_size, float sigmax, float sigmay, float theta);

// cost function for the given region. (xstop, ystop) is not inclusive by (xstart, ystart) is
float cost(float* eimg, int ew, int eh, int offset, float sigma_est,
        int xstart, int ystart, int xstop, int ystop,
        float sigmax, float sigmay, float theta, int filt_size);

// compute derivative w.r.t sigma-x of the cost function for the given region. (xstop, ystop) is not inclusive by (xstart, ystart) is
float costderivx(float *eimg, int ew, int eh, int offset, float sigma_est,
        int xstart, int ystart, int xstop, int ystop,
        float sigmax, float sigmay, float theta, int filt_size);

// compute derivative w.r.t sigma-y of the cost function for the given region. (xstop, ystop) is not inclusive by (xstart, ystart) is
float costderivy(float *eimg, int ew, int eh, int offset, float sigma_est,
        int xstart, int ystart, int xstop, int ystop,
        float sigmax, float sigmay, float theta, int filt_size);


#endif
