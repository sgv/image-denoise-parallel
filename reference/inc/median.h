#ifndef __QUICK_SELECT__
#define __QUICK_SELECT__

// computes the median of arr[]. This function works in place (it modifies the contents of arr[]).
float median(float arr[], int size);

#endif
