from PIL import Image
import math
import numpy as np

def imgFromPixels(pixels):
    width = len(pixels[0])
    height = len(pixels)

    im = Image.new('L', (width, height))

    for x in range(width):
        for y in range(height):
            im.putpixel( (x,y), pixels[y][x] )
    
    return im

def imagesIdentical(im1, im2):
    try:
        w,h = im1.size
        w2,h2 = im2.size
        
        if w != w2 or h != h2:
            return False

        for x in range(w):
            for y in range(h):
                if im1.getpixel((x,y)) != im2.getpixel((x,y)):
                    return False
        return True
    except:
        return False

def psnr(im1, im2):
    ''' returns PSNR in dB '''
    w, h = im1.size
    w2, h2 = im2.size

    if w != w2 or h != h2:
        return None

    mse = 0
    im1_np = np.array(im1).astype(np.float)
    im2_np = np.array(im2).astype(np.float)
    mse = np.sum( (im1_np - im2_np) * (im1_np - im2_np) )
    mse = (mse * 1.0) / (w * h)

    if mse < 1e-4:
        return float("inf")

    psnr = 10.0 * math.log10(255.0**2/mse)

    return psnr

def addawgn8b(im, sigma):
    ''' Add AWGN noise to 8-bit unsigned images '''
    w, h = im.size
    orig_np = np.array(im)
    if orig_np.dtype != "uint8":
        print "Error ! Not an unsigned image"
        return None
    orig_np_f = orig_np.astype(np.float)
    noisy_np = orig_np_f + sigma * np.random.randn(w, h)
    noisy_np[noisy_np < 0] = 0
    noisy_np[noisy_np > 255] = 255
    noisy_np = np.round(noisy_np).astype(np.uint8)
    return Image.fromarray(noisy_np)

def addawpn8b(im, lam):
    ''' Add additive white poisson noise to 8-bit unsigned images '''
    w, h = im.size
    orig_np = np.array(im)
    if orig_np.dtype != "uint8":
        print "Error ! Not an unsigned image"
        return None
    orig_np_f = orig_np.astype(np.float)
    noisy_np = orig_np_f + np.random.poisson(lam, (w, h)) - lam
    noisy_np[noisy_np < 0] = 0
    noisy_np[noisy_np > 255] = 255
    noisy_np = np.round(noisy_np).astype(np.uint8)
    return Image.fromarray(noisy_np)


def addawln8b(im, sigma):
    ''' Add additive white Laplace noise to 8-bit unsigned images '''
    w, h = im.size
    orig_np = np.array(im)
    if orig_np.dtype != "uint8":
        print "Error ! Not an unsigned image"
        return None
    orig_np_f = orig_np.astype(np.float)
    noisy_np = orig_np_f + np.random.laplace(loc = 0, scale = sigma / 2**0.5, size = im.size)
    noisy_np[noisy_np < 0] = 0
    noisy_np[noisy_np > 255] = 255
    noisy_np = np.round(noisy_np).astype(np.uint8)
    return Image.fromarray(noisy_np)

