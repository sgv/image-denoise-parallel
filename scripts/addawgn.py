import imghelper
from PIL import Image

import sys

def usage():
    print "This program adds Additive White Gaussian Noise (AWGN) to images"
    print "Usage: python addawgn.py <original_input_image> <noise_std_deviation> <noisy_output_image>"
    exit(1)

def main():
    if len(sys.argv) != 4:
        print usage()
    orig = Image.open(sys.argv[1])
    noisy = imghelper.addawgn8b(orig, float(sys.argv[2]))
    noisy.save(sys.argv[3])

if __name__ == '__main__':
    main()
