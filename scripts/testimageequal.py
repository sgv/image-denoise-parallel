from PIL import Image
from imghelper import *

def main():
    im1 = Image.open('testimages/test.png')
    im2 = Image.open('testimages/test.png')
    if imagesIdentical(im1, im2):
        print "Images are identical"
        return 0
    else:
        print "Images are different"
        return 1

if __name__ == '__main__':
    main()
