import imghelper
from PIL import Image
import sys

N = 20

def usage():
    print "This program measures PSNR of images in dB"
    print "Usage: python psnr.py <orig_image_path> <corrupt_image_dir>"
    print ""
    sys.exit(-1)

def main():
    if len(sys.argv) != 3:
        print usage()
    
    ext = '.' + sys.argv[1].split('.')[-1]
    orig = Image.open(sys.argv[1])
    corrupt_image_dir = sys.argv[2]
    if corrupt_image_dir[-1] != '/':
        corrupt_image_dir += '/'

    psnr_avg = 0

    for i in range(N):
        corrupt_path_i = corrupt_image_dir + str(i) + ext
        psnr_avg += imghelper.psnr(Image.open(sys.argv[1]), Image.open(corrupt_path_i))

    print psnr_avg/N, 'dB'

if __name__ == '__main__':
    main()
