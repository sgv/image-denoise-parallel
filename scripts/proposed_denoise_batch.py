import sys
import os
import subprocess

N = 20
sigmas = [10, 20, 50, 100]
args = "-f 9 -b 8 -a 4" # command line arguments to the denoising executable

def main():
    for sigma in sigmas:
        print "Sigma: ", sigma
        for i in range(N):
            print "Iter ", i

            corruptPath='/home/sagar/Desktop/image-denoie-parallel/testimages/corrupt/cameraman_awln_sigma'+str(sigma)+'/'+str(i)+'.tif'
            denoisedPath='/home/sagar/Desktop/image-denoie-parallel/testimages/proposed_denoised/cameraman_awln_sigma'+str(sigma)+'/'+str(i)+'.tif'

            if os.system('/home/sagar/Desktop/image-denoie-parallel/GPU/imgdenoise'+' '+args+' -o '+denoisedPath+' '+corruptPath) != 0:
                print "Error !"
                sys.exit(11)


if __name__ == '__main__':
    main()
