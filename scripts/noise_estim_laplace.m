clear all; close all;

sigma_act = 50;
N = 20;

x = double(imread('../testimages/original/barbara.png'));
h = 1/9 .* [-1 -1 -1;-1 8 -1;-1 -1 -1];
sigma_est = zeros(N,1);

for it=1:N
    %y = x + laprnd(size(x,1),size(x,2),0,sigma_act);
    y = x + sigma_act * randl(size(x));
    %y = 127*ones(size(x)) + sigma_act * randl(size(x));
    %y = 127*ones(size(x)) + laprnd(size(x,1),size(x,2),0,sigma_act);
    yf = conv2(y, h);
    %sigma_est(it) = median(abs(yf(:))) / 0.5177;
    sigma_est(it) = median(abs(yf(:))) / 0.4901;
end

sum(sigma_est(:))/N