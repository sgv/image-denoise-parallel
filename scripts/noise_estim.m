clear all; close all;

sigma_act = 0;
N = 20;

x = double(imread('../testimages/boat.tif'));
h = 1/9 .* [-1 -1 -1;-1 8 -1;-1 -1 -1];
sigma_est = zeros(N,1);

for it=1:N
    y = x + sigma_act * randn(size(x));
    yf = conv2(y, h);
    sigma_est(it) = median(abs(yf(:))) / 0.6745;
end

sum(sigma_est(:))/N