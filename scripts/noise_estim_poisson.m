sigma = 50;
variance = sigma ^ 2;

lambda = variance;
b = -lambda; % bias term to remove DC in poisson noise

N = 20;

x = double(imread('../testimages/boat.tif'));
h = 1/9 .* [-1 -1 -1;-1 8 -1;-1 -1 -1];
sigma_est = zeros(N,1);

for it=1:N
    y = x + poissrnd(lambda, size(x)) + b;
    yf = conv2(y, h);
    sigma_est(it) = median(abs(yf(:))) / 0.6745;
end

sum(sigma_est(:))/N