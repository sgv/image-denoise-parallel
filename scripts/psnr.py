import imghelper
from PIL import Image
import sys

def usage():
    print "This program measures PSNR of images in dB"
    print "Usage: python psnr.py <orig_image_path> <corrupt_image_path>"
    print ""
    sys.exit(-1)

def main():
    if len(sys.argv) != 3:
        print usage()
    print imghelper.psnr(Image.open(sys.argv[1]), Image.open(sys.argv[2]))

if __name__ == '__main__':
    main()
