import imghelper
from PIL import Image

import sys

def usage():
    print "This program adds Additive White Poisson Noise (AWPN) to images"
    print "Usage: python addawpn.py <original_input_image> <noise_std_deviation> <noisy_output_image>"
    exit(1)

def main():
    if len(sys.argv) != 4:
        print usage()
    orig = Image.open(sys.argv[1])
    sigma = float(sys.argv[2])
    lam = sigma * sigma
    noisy = imghelper.addawpn8b(orig, lam)
    noisy.save(sys.argv[3])

if __name__ == '__main__':
    main()
