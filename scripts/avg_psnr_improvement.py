import sys
import os
import subprocess

# Important !!!!
# run this script from the directory that contains the 'imgdenoise' executable


N = 20 # average PSNR over 'N' realizations of noise
args = "-f 9 -b 8 -a 4" # command line arguments to the denoising executable


def usage():
    print "This program computes the average improvement in PSNR over %s realizations of noise" % N
    print "Usage: python avg_psnr_improvement.py <noise_free_image_path> <awgn_std_dev>"
    print ""



def main():
    if len(sys.argv) != 3:
        usage()
        sys.exit(1)

    noise_level = float(sys.argv[2])
    orig_img_path = sys.argv[1]
    orig_img_path_without_ext = orig_img_path[:-len(orig_img_path.split('.')[-1])-1]
    ext = orig_img_path.split('.')[-1]
    script_path = sys.argv[0][:-len(os.path.basename(__file__))]
    add_awgn_script_path = script_path + 'addnoise.py'
    measure_psnr_script_path = script_path + 'psnr.py'
    denoise_path = './imgdenoise'
    
    noisy_img_path_without_ext = orig_img_path_without_ext + "_sigma"+str(int(noise_level))
    denoised_img_path_without_ext = noisy_img_path_without_ext + "_denoised"

    noisy_img_path = noisy_img_path_without_ext + '.' + ext
    denoised_img_path = denoised_img_path_without_ext + '.' + ext
    
    avg_input_psnr = 0
    avg_output_psnr = 0

    for i in range(N):
        print "Iteration %s" % i

        print "Adding AWGN with std.dev %s to the original image . . ." % noise_level
        if os.system('python '+add_awgn_script_path+' '+orig_img_path+' '+str(noise_level)+' '+noisy_img_path) != 0:
            print "Error !"
            sys.exit(10)
        
        print "Denoising the image . . ."
        if os.system(denoise_path+' '+args+' -o '+denoised_img_path+' '+noisy_img_path) != 0:
            print "Error !"
            sys.exit(11)
        
        cmd = ['python',measure_psnr_script_path, orig_img_path, noisy_img_path]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        out, err = p.communicate()
        if err:
            print "Error ! Error-code = ", err
            print out
            sys.exit(2)
        input_psnr = float(out)
        print "Input PSNR = ", input_psnr, 'dB'

        cmd = ['python',measure_psnr_script_path, orig_img_path, denoised_img_path]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        out, err = p.communicate()
        if err:
            print "Error ! Error-code = ", err
            print out
            sys.exit(2)
        output_psnr = float(out)
        print "Output PSNR = ", output_psnr, 'dB'

        avg_input_psnr += input_psnr
        avg_output_psnr += output_psnr

        print "--------------------"

    avg_input_psnr /= N
    avg_output_psnr /= N
    print ";;;;;;;;;;;;;;;;;;;;;;;;;;;;"

    print "Noise-std-deviation = ", noise_level
    print "Avg. Input-PSNR = ", avg_input_psnr, 'dB'
    print "Avg. Output-PSNR = ", avg_output_psnr, 'dB'


if __name__ == '__main__':
    main()
