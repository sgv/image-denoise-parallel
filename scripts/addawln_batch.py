import imghelper
from PIL import Image

import sys

N = 20

def usage():
    print "This program adds Additive White Laplace Noise (AWLN) to images"
    print "Usage: python addawln_batch.py <original_input_image> <noise_std_deviation> <noisy_output_image_directory>"
    exit(1)

def main():
    if len(sys.argv) != 4:
        print usage()
    
    orig_path = sys.argv[1]
    orig = Image.open(orig_path)

    targ = sys.argv[3]
    if targ[-1] != '/':
        targ = targ + '/'
    
    orig_img_path_without_ext = orig_path[:-len(orig_path.split('.')[-1])-1]
    ext = orig_path.split('.')[-1]

    for i in range(N):
        noisy = imghelper.addawln8b(orig, float(sys.argv[2]))
        noisy.save(targ + str(i) + '.' + ext)

if __name__ == '__main__':
    main()
