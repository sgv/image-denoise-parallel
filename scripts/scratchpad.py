from imghelper import *

def main():
    im1 = Image.open("testimages/test.png")
    im2 = Image.open("testimages/test_denoised.png")
    print "PSNR: ", psnr(im1, im2), 'dB'

if __name__ == '__main__':
    main()
