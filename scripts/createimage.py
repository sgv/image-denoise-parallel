from PIL import Image
from imghelper import *

def main():
    pixels = [
            [1  ,2  ,3  ,4  ],
            [10 ,20 ,30 ,40 ],
            [100,200,200,200],
            [200,220,240,250]
        ]
    im = imgFromPixels(pixels)
    im.save('testimages/test.png','png')


if __name__ == '__main__':
    main()
